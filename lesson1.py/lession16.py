
import sys 
words = {}
file_with_word_path = sys.argv[1]
with open(file_with_word_path) as f:
    for word in f.readlines():
        word_key = tuple(sorted(word))
        if word_key in words:
            words[word_key].append(word.strip())
        else:
            words[word_key] = []
            words[word_key].append(word.strip())

for word_key in words:
    print(' '.join(words[word_key]))    