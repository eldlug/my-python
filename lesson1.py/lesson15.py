import sys 
hosts = {}

with open("/home/student/Projects/Course/python_lesson1/lesson1.py/hosts.txt") as f:
    for line in f.readlines():
        host = line.split("=")
        print(line,host)
        hosts[host[0].strip()] = host[1].strip()

for host in sys.argv[1:]:
    if host in hosts:
        print(host, ' - ',hosts[host])
    else:
        print("Error, ",host ,"is unknow")