#!/usr/bin/env python3
################
import sys


grades_sum = 0

grades = []

print(sys.argv)
for grade in sys.argv[1:]:
   
    grade = int(grade)
    grades_sum = grades_sum + grade
    grades.append(grade)

for grade in grades:
    if grades_sum/len(grades) < grade:
        print(grade)