#!/usr/bin/env python3
#
#purpose: inheritance in Python
#data: 15.01.2021
###################

# parent class

class ThinkBe():
	def __init__(self, name):
		self.name = name
		print(f"I,{name} think, therefore I am")

#child class 1

class Animal(ThinkBe):
	def __init__(self, name):
		ThinkBe.__init__(self, name)
	def home_animal(self, home):
		print("For pets press=1, for wild animal press = 2")
		self.home = home
		print(self.home)
		if self.home == 1:
			print("You have a great pet!")
		elif self.home == 2:
			print("This is a wild animal! Run!")

#child class 2

class Robots(ThinkBe):
	def __init__(self, name):
		ThinkBe.__init__(self, name)
		print("Type your brand & build year")
	def brand (self, brand, year)
		self.brand = brand
		self.year = year
		print(f"Your brand is {self.brand}, you were created in {self.year}")

