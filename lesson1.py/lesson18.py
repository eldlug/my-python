#!/usr/bin/env python3
#
# new class, each component may depend on other components
# components construction depends on other components
#
#data 11.01.2021
##################

class Widget:
	def __init__(self, name):
		self.name = name
		self.dependency = []
		self.built = False
	def add_dependency(self, *dependency):
		self.dependency.extend(dependency)
	def build(self):
		self.built = True
		for d in self.dependency:
			if not d.built:
				d.build()
				print(d.name)

